﻿using PostalService.Base.Classes;
using PostalService.Base.Models;
using PostalService.WPFClient.AuthorizationModule.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System.Text.RegularExpressions;

namespace PostalService.WPFClient.AuthorizationModule.ViewModels
{
    public class AuthorizationViewModel : BindableBase, INavigationAware
    {
        private readonly IRegionManager _regionManager;
        public AuthorizationViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;
            SingInCommand = new DelegateCommand<string>(OnSingIn);
            RegistrationCommand = new DelegateCommand(OnRegistration);
            IsUnregisteredUser = false;
        }

        private bool _isUnregisteredUser;
        public bool IsUnregisteredUser
        {
            get { return _isUnregisteredUser; }
            set { SetProperty(ref _isUnregisteredUser, value); }
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set { SetProperty(ref _userName, value); }
        }

        private string _incorrectLogin;
        public string IncorrectLogin
        {
            get { return _incorrectLogin; }
            set { SetProperty(ref _incorrectLogin, value); }
        }

        public DelegateCommand<string> SingInCommand { get; private set; }

        private async void OnSingIn(string userName)
        {
            if (userName != string.Empty)
            {
                string pattern = @"^[a-zA-Z][a-zA-Z0-9-_\.]{3,20}$";
                if (Regex.IsMatch(userName, pattern))
                {
                    string uri = string.Format("https://localhost:5001/api/users/used/{0}", userName);
                    if (await UserHttpClient<User>.IsUserRegistered(uri))
                    {
                        var parameters = new NavigationParameters();
                        parameters.Add("userName", userName);
                        _regionManager.RequestNavigate(RegionNames.WorkingRegion, ViewsName.PostalView, parameters);
                    }
                    else
                    {
                        IsUnregisteredUser = true;
                        IncorrectLogin = userName;
                    }
                }
            }
            //List<User> user =  await UserHttpClient<User>.GetItems("https://localhost:5001/api/users");  
        }

        public DelegateCommand RegistrationCommand { get; private set; }

        private void OnRegistration()
        {
            RegistrationView view = new RegistrationView();
            view.ShowDialog();
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            UserName = string.Empty;
            IncorrectLogin = string.Empty;
            IsUnregisteredUser = false;
        }
    }
}
