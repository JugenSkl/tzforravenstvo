﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Mvvm;
using Prism.Commands;
using PostalService.Base.Models;
using PostalService.Base.Classes;
using System.Windows;
using System.ComponentModel;

namespace PostalService.WPFClient.AuthorizationModule.ViewModels
{
    public class RegistrationViewModel:BindableBase
    {
        public RegistrationViewModel()
        {
            SingUpCommand = new DelegateCommand<Window>(OnSingUp);
            this.PropertyChanged += UserPropertyChanged;
        }

        private string _username;
        public string UserName
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set { SetProperty(ref _firstName, value); }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set { SetProperty(ref _lastName, value); }
        }

        private bool _isValidationSuccessful;
        public bool IsValidationSuccessful
        {
            get { return _isValidationSuccessful; }
            set { SetProperty(ref _isValidationSuccessful, value); }
        }

        private string _UserNameBusy;
        public string UserNameBusy
        {
            get { return _UserNameBusy; }
            set { SetProperty(ref _UserNameBusy, value); }
        }

        private bool _isUsed;
        public bool IsUsed
        {
            get { return _isUsed; }
            set { SetProperty(ref _isUsed, value); }
        }

        public DelegateCommand<Window> SingUpCommand { get; private set; }

        private async void OnSingUp(Window window)
        {
            string uri = string.Format("https://localhost:5001/api/users/used/{0}", UserName);
            if (!await UserHttpClient<User>.IsUserRegistered(uri))
            {
                User user = new User()
                {
                    UserName = UserName,
                    LastName = LastName,
                    FirstName = FirstName,
                };
                if (await UserHttpClient<User>.AddItemInServis("api/users", user))
                {
                    window.DialogResult = true;
                }
            }
            else
            {
                IsUsed = true;
                UserNameBusy = UserName;
            }

            
        }

        void UserPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (UserName != null &&
               LastName != null &&
               FirstName != null)
            {
                IsValidationSuccessful = true;
            }
            else
            {
                IsValidationSuccessful = false;
            }
        }
    }
}
