﻿using PostalService.Base.Classes;
using PostalService.WPFClient.AuthorizationModule.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PostalService.WPFClient.AuthorizationModule
{
    public class AuthorizationModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion(RegionNames.WorkingRegion, typeof(AuthorizationView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<AuthorizationModule>();
        }
    }
}
