using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using PostalService.WebService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Routing;

namespace PostalService.WebService
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            string con = "Server=(localdb)\\mssqllocaldb;Database=PostalServicdbstore;Trusted_Connection=True";

            services.AddDbContext<WebServiceContext>(opt => opt.UseSqlServer(con));

            services.AddControllers();
        }


        public void Configure(IApplicationBuilder app)
        {

            app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
