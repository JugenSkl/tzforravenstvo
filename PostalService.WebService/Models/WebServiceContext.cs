﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PostalService.Base.Models;

namespace PostalService.WebService.Models
{
    public class WebServiceContext : DbContext
    {
        public DbSet<Letter> Letters { get; set; }

        public DbSet<User> Users { get; set; }

        public WebServiceContext(DbContextOptions<WebServiceContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
