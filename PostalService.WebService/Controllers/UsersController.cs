﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PostalService.Base.Models;
using PostalService.WebService.Models;

namespace PostalService.WebService.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {

        public UsersController(WebServiceContext context)
        {
            dataBase = context;
        }

        WebServiceContext dataBase;

        // GET: api/<controller>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> Get()
        {
            return await dataBase.Users.ToListAsync();
        }

        // GET api/<controller>/5
        [HttpGet("{name}")]
        public async Task<ActionResult<User>> Get(string name)
        {
            User user = await dataBase.Users.FirstOrDefaultAsync(_ => _.UserName == name);
            if (user == null)
                return NotFound();
            return new ObjectResult(user);
        }

        [HttpGet("{name}/inbox")]
        public async Task<ActionResult<IEnumerable<Letter>>> GetInbox(string name)
        {
            IEnumerable<Letter> letters = await Task.Run(() => dataBase.Letters.Include(_ => _.Sender).Include(_ => _.Addressee).Where(_ => _.Addressee.UserName == name));
            if (letters == null)
                return NotFound();
            return new ObjectResult(letters);
        }

        [HttpGet("{name}/sent")]
        public async Task<ActionResult<IEnumerable<Letter>>> GetSent(string name)
        {
            IEnumerable<Letter> letters = await Task.Run(() => dataBase.Letters.Include(_ => _.Sender).Include(_ => _.Addressee).Where(_ => _.Sender.UserName == name));
            if (letters.Count() == 0)
                return NotFound();
            return new ObjectResult(letters);
        }

        [HttpGet("used/{name}")]
        public async Task<ActionResult<string>> IsUserRegistered(string name)
        {
            if (!dataBase.Users.Any())
            {
                return NotFound("name");
            }
            User user = await dataBase.Users.FirstOrDefaultAsync(_ => _.UserName == name);
            if (user != null)
                return Ok("name");
            return NotFound("name");
        }

        // POST api/<controller>
        [HttpPost]
        public async Task<ActionResult<User>> Post([FromBody]User user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            dataBase.Users.Add(user);
            await dataBase.SaveChangesAsync();
            return Ok(user);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<User>> Put([FromBody]User user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            if (!dataBase.Letters.Any(_ => _.Id == user.Id))
            {
                return NotFound();
            }
            dataBase.Update(user);
            await dataBase.SaveChangesAsync();
            return Ok(user);
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> Delete(int id)
        {
            User user = await dataBase.Users.FirstOrDefaultAsync(_ => _.Id == id);
            if (user == null)
                return NotFound();
            dataBase.Users.Remove(user);
            await dataBase.SaveChangesAsync();
            return Ok(user);
        }
    }
}
