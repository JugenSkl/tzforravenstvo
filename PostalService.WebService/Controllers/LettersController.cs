﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PostalService.WebService.Models;
using PostalService.Base.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PostalService.WebService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LettersController : ControllerBase
    {
        WebServiceContext dataBase;

        public LettersController(WebServiceContext context)
        {
            dataBase = context;
        }
        

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Letter>>> Get()
        {
            return await dataBase.Letters.Include(_=> _.Addressee).Include(_ => _.Sender).ToListAsync();
        }

        // GET api/letters/user1
        [HttpGet("{id}")]
        public async Task<ActionResult<Letter>> Get(int id)
        {
            Letter letter = await dataBase.Letters.Include(_ => _.Addressee)
                                                  .Include(_ => _.Sender)
                                                  .FirstOrDefaultAsync(_ => _.Id == id);
            if (letter == null)
                return NotFound();
            return new ObjectResult(letter);
        }

        // POST api/letters
        [HttpPost]
        public async Task<ActionResult<Letter>> Post([FromBody]Letter letter)
        {
            if (letter == null)
            {
                return BadRequest();
            }

            dataBase.Letters.Add(new Letter()
            {
                Title = letter.Title,
                Contents = letter.Contents,
                Date = letter.Date,
                Addressee = dataBase.Users.First(_ => _.UserName == letter.Addressee.UserName),
                Sender = dataBase.Users.First(_ => _.UserName == letter.Sender.UserName),
            });
            await dataBase.SaveChangesAsync();
            return Ok(letter);
        }

        // PUT api/letters/
        [HttpPut]
        public async Task<ActionResult<Letter>> Put([FromBody]Letter letter)
        {
            if (letter == null)
            {
                return BadRequest();
            }
            if(!dataBase.Letters.Any(_ => _.Id == letter.Id))
            {
                return NotFound();
            }
            dataBase.Update(new Letter()
            {
                Title = letter.Title,
                Contents = letter.Contents,
                Date = letter.Date,
                Addressee = dataBase.Users.First(_ => _.UserName == letter.Addressee.UserName),
                Sender = dataBase.Users.First(_ => _.UserName == letter.Sender.UserName),
            });
            await dataBase.SaveChangesAsync();
            return Ok(letter);

        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Letter>> Delete(int id)
        {
            Letter letter = await dataBase.Letters.FirstOrDefaultAsync(_ => _.Id == id);
            if (letter == null)
                return NotFound();
            dataBase.Letters.Remove(letter);
            await dataBase.SaveChangesAsync();
            return Ok(letter);
        }
    }
}
