﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PostalService.Base.Models;
using PostalService.Base.Classes;
using System.Text;
using Prism.Mvvm;
using Prism.Commands;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Prism.Regions;
using System.ComponentModel;

namespace PostalService.WPFClient.PostalModule.ViewModels
{
    public class PostalViewModel: BindableBase, INavigationAware
    {
        private readonly IRegionManager _regionManager;
        public PostalViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;
            Users = new ObservableCollection<User>();
            OpenInboxCommand = new DelegateCommand(OnOpenInbox);
            OpenSendItemsCommand = new DelegateCommand(OnSendIntems);
            LoguotCommand = new DelegateCommand(OnLoguot);
            CanselCommand = new DelegateCommand(OnCansel);
            SentToCommand = new DelegateCommand(OnSentTo);
            RemoveCommand = new DelegateCommand<Letter>(OnRemove);
            NewLetterCommand = new DelegateCommand(OnNewLetter);
            SelectedLetterCommand = new DelegateCommand<Letter>(OnSelectedLetter);
            SelectedLetter = new Letter();
            Letters = new ObservableCollection<Letter>();
            IsValidationSuccessful = false;
        }

        private string uri;

        private User _user;
        public User User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get { return _users; }
            set { SetProperty(ref _users, value); }
        }

        private ObservableCollection<Letter> _letters;
        public ObservableCollection<Letter> Letters
        {
            get { return _letters; }
            set { SetProperty(ref _letters, value); }
        }

        private Letter _selectedLetter;
        public Letter SelectedLetter
        {
            get { return _selectedLetter; }
            set 
            { 
                if(_selectedLetter != null)
                {
                    _selectedLetter.PropertyChanged -= UserPropertyChanged;
                }
                SetProperty(ref _selectedLetter, value);
                if (_selectedLetter != null)
                {
                    _selectedLetter.PropertyChanged += UserPropertyChanged;
                }
            }
        }

        private string _typeLetters;
        public string TypeLetters
        {
            get { return _typeLetters; }
            set { SetProperty(ref _typeLetters, value); }
        }

        private string _typeSelectedLetter;
        public string TypeSelectedLetter
        {
            get { return _typeSelectedLetter; }
            set { SetProperty(ref _typeSelectedLetter, value); }
        }

        private bool _isDownloaded;
        public bool IsDownloaded
        {
            get { return _isDownloaded; }
            set { SetProperty(ref _isDownloaded, value); }
        }

        private bool _isValidationSuccessful;
        public bool IsValidationSuccessful
        {
            get { return _isValidationSuccessful; }
            set { SetProperty(ref _isValidationSuccessful, value); }
        }

        public DelegateCommand OpenInboxCommand { get; private set; }
        private async void OnOpenInbox()
        {
            List<Letter> letters = new List<Letter>();
            letters = await UserHttpClient<Letter>.GetItems(uri + "/inbox");
            Letters = new ObservableCollection<Letter>(letters);
            TypeLetters = "received ";
            SelectedLetter = Letters.Count > 0 ? Letters[Letters.Count - 1] : null;
            TypeSelectedLetter = "Selected";
        }

        public DelegateCommand LoguotCommand { get; private set; }
        private async void OnLoguot()
        {
            Settings.UserName = string.Empty;
            _regionManager.RequestNavigate(RegionNames.WorkingRegion, ViewsName.AuthorizationView);
        }

        public DelegateCommand OpenSendItemsCommand { get; private set; }
        private async void OnSendIntems()
        {
            TypeLetters = "sent";
            List<Letter> letters = new List<Letter>();
            letters = await UserHttpClient<Letter>.GetItems(uri + "/sent");
            Letters = new ObservableCollection<Letter>(letters);
            SelectedLetter = Letters.Count > 0 ? Letters[Letters.Count-1] : null;
            TypeSelectedLetter = "Selected";
        }

        public DelegateCommand NewLetterCommand { get; private set; }
        private void OnNewLetter()
        {
            SelectedLetter = new Letter();
            TypeSelectedLetter = "New";
        }

        public DelegateCommand SentToCommand { get; private set; }
        private async void OnSentTo()
        {
            SelectedLetter.Date = DateTime.Now;
            SelectedLetter.Sender = User;
            bool result = await UserHttpClient<Letter>.AddItemInServis("https://localhost:5001/api/letters", SelectedLetter);
            if(result)
            {
                OnSendIntems();
            }
        }

        public DelegateCommand CanselCommand { get; private set; }
        private void OnCansel()
        {
            SelectedLetter = Letters.Count > 0 ? Letters[Letters.Count - 1] : null;
            TypeSelectedLetter = "Selected";
        }

        public DelegateCommand<Letter> RemoveCommand { get; private set; }
        private async void OnRemove(Letter letter)
        {
            if (await UserHttpClient<Letter>.RemoveItemInServis("https://localhost:5001/api/letters/" + letter.Id))
            {
                OnOpenInbox();
            }
        }

        public DelegateCommand<Letter> SelectedLetterCommand { get; private set; }
        private void OnSelectedLetter(Letter letter)
        {
            SelectedLetter = letter;
            TypeSelectedLetter = "Selected";
        }

        public async void OnNavigatedTo(NavigationContext navigationContext)
        {
            string name = navigationContext.Parameters["userName"] as string;
            if (name != null)
            {
                uri = string.Format("https://localhost:5001/api/users/{0}", name);
                User = await UserHttpClient<User>.GetItem(uri);
                List<User> users = await UserHttpClient<User>.GetItems("https://localhost:5001/api/users");
                users.Remove(users.FirstOrDefault(_ => _.Id == User.Id));
                Users.Clear();
                Users.AddRange(users);
                IsDownloaded = true;
                OnOpenInbox();
            }
                
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            uri = string.Empty;
            User = null;
            Letters = new ObservableCollection<Letter>();
        }

        void UserPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (SelectedLetter.Contents != null &&
               SelectedLetter.Title != null &&
               SelectedLetter.Addressee != null)
            {
                IsValidationSuccessful = true;
            }
            else
            {
                IsValidationSuccessful = false;
            }
        }
    }
}
