﻿using System;
using System.Collections.Generic;
using System.Text;
using PostalService.Base.Classes;
using PostalService.WPFClient.PostalModule.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PostalService.WPFClient.PostalModule
{
    public class PostalModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {

        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<PostalView>();
        }
    }
}
