﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostalService.Base.Classes
{
    public static class RegionNames
    {
        public const string WorkingRegion = "WorkingRegion";
        public const string LeftRegion = "LeftRegion";
        public const string BottomRegion = "BottomRegion";
    }
}
