﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using PostalService.Base.Models;

namespace PostalService.Base.Classes
{
    public class UserHttpClient<T> where T : new()
    {
        static UserHttpClient()
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            _client = new HttpClient(clientHandler);
            _client.BaseAddress = new Uri("https://localhost:5001/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private static HttpClient _client;

        public static async Task<List<T>> GetItems(string uri)
        {
            HttpResponseMessage response = await _client.GetAsync(uri);
            List<T> result = new List<T>();
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<List<T>>();
            }
            return result;
        }

        public static async Task<T> GetItem(string uri)
        {
            HttpResponseMessage response = await _client.GetAsync(uri);
            T result = new T();
            if (response.IsSuccessStatusCode)
            {
                var Str = await response.Content.ReadAsStringAsync();
                result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(Str);
            }
            return result;
        }

        public static async Task<bool> IsUserRegistered(string uri)
        {
            HttpResponseMessage response = await _client.GetAsync(uri);
            return response.IsSuccessStatusCode;
        }

        public static async Task<bool> AddItemInServis(string uri, T value)
        {
            string content = System.Text.Json.JsonSerializer.Serialize<T>(value);
            HttpContent inputContent = new StringContent(content, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PostAsync(uri, inputContent);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                return true;
            else
                return false;
        }

        public static async Task<bool> RemoveItemInServis(string uri)
        {
            HttpResponseMessage response = await _client.DeleteAsync(uri);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                return true;
            else
                return false;
        }
    }
}
