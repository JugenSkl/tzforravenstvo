﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostalService.Base.Classes
{
    public static class ViewsName
    {
        public const string PostalView = "PostalView";
        public const string AuthorizationView = "AuthorizationView";
    }
}
