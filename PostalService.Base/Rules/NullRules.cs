﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace PostalService.Base.Rules
{
    public class NullRules : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if(value != null)
            {
                return new ValidationResult(true, null);
            }
            else
            {
                return new ValidationResult(false, "The value is null");
            }
        }
    }
}
