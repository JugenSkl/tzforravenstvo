﻿using PostalService.Base.Classes;
using PostalService.Base.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PostalService.Base.Rules
{
    public class LoginRules : ValidationRule
    {
        bool result = false;
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value != null)
            {
                string name = value as string;
                if (name.Length > 3)
                {
                    if (name == string.Empty)
                        return new ValidationResult(true, null);
                    string pattern = @"^[a-zA-Z][a-zA-Z0-9-_\.]{3,20}$";
                    if (Regex.IsMatch(name, pattern))
                    {
                            return new ValidationResult(true, null);
                    }
                    else
                    {
                        return new ValidationResult(false, "Login contains illegal characters: /*;:%$!?#(){}|+-..,");
                    }
                }
                else
                {
                    return new ValidationResult(false, "The value should be more than 3 characters");
                }

            }
            return new ValidationResult(false, "The value is null");
        }
    }
}
