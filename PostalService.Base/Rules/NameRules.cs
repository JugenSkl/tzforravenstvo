﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace PostalService.Base.Rules
{
    public class NameRules : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if(value != null)
            {
                string name = value as string;
                if (name.Length > 1)
                {
                    string pattern = @"^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я]+$";
                    if (Regex.IsMatch(name, pattern))
                    {
                        return new ValidationResult(true, null);
                    }
                    else
                    {
                        return new ValidationResult(false, "The value contains illegal characters: 1-9/*;:%$!?#(){}|+-..,");
                    }
                }
                else
                {
                    return new ValidationResult(false, "The value should be more than 1 characters");
                }

            }
            else
            {
                return new ValidationResult(false, "The value is null");
            }
        }
    }
}
