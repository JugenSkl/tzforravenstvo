﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace PostalService.Base.Models
{
    public class Letter: BindableBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        private string _title;
        public string Title 
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
        private DateTime _date;
        public DateTime Date
        {
            get { return _date; }
            set { SetProperty(ref _date, value); }
        }
        private User _addressee;
        public User Addressee
        {
            get { return _addressee; }
            set { SetProperty(ref _addressee, value); }
        }
        private User _sender;
        public User Sender
        {
            get { return _sender; }
            set { SetProperty(ref _sender, value); }
        }
        private string _contents;
        public string Contents
        {
            get { return _contents; }
            set { SetProperty(ref _contents, value); }
        }
    }
}
