﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Mvvm;

namespace PostalService.Base.Models
{
    public class User: BindableBase
    {
        public User()
        {

        }

        public int Id { get; set; }

        private string _username;
        public string UserName
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set { SetProperty(ref _firstName, value); }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set { SetProperty(ref _lastName, value); }
        }

    }
}
